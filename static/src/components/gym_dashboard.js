/** @odoo-module */

import { registry } from "@web/core/registry"
import { Card } from "./cards/card"
import { loadJS } from "@web/core/assets"
import { useService } from "@web/core/utils/hooks"
const { Component, onWillStart, useRef, onMounted, useState } = owl

export class GymDashboard extends Component {
    setup(){
        this.state = useState({
            memberinquiry: {
                value: 10,
                percentage:6,
            },
            memberconfirm: {
                value: 10,
                percentage:6,
            },
            period:90,
        })

        this.orm = useService("orm")
        this.actionService = useService("action")

        onWillStart(async () => {
            this.getDates()
            await this.getInquiryMember()
            await this.getConfirmMember()
            await this.getSales()
            await this.getPurchase()
        })
    }

    async onChangePeriod(){
        this.getDates()
        await this.getInquiryMember()
        await this.getConfirmMember()
        await this.getSales()
        await this.getPurchase()
    }

    getDates(){
        let date = new Date();
        let cday = date.getDate() - this.state.period;
        let cdate = new Date(date.setDate(cday));
        const cyyyy = cdate.getFullYear();
        let cmm = cdate.getMonth() + 1; // Months start at 0!
        let cdd = cdate.getDate();
        if (cdd < 10) cdd = '0' + cdd;
        if (cmm < 10) cmm = '0' + cmm;
        this.state.current_date = cdd + '/' + cmm + '/' + cyyyy;

        let pday = date.getDate() - (this.state.period * 2);
        let pdate = new Date(date.setDate(pday));
        const pyyyy = pdate.getFullYear();
        let pmm = pdate.getMonth() + 1; // Months start at 0!
        let pdd = pdate.getDate();
        if (pdd < 10) pdd = '0' + pdd;
        if (pmm < 10) pmm = '0' + pmm;
        this.state.previous_date = pdd + '/' + pmm + '/' + pyyyy;
    }

    async getInquiryMember(){
        let domain = [['member_status', 'in', ['Interested', 'Rejected']]]
        if (this.state.period > 0)
        {
            domain.push(['date', '>', this.state.current_date])
        }
        const data = await this.orm.searchCount("inquiry", domain)
        this.state.memberinquiry.value = data

        // previous period
        let prev_domain = [['member_status', 'in', ['Interested', 'Rejected']]]
        if (this.state.period > 0){
            prev_domain.push(['date','>', this.state.previous_date], ['date','<=', this.state.current_date])
        }
        const prev_data = await this.orm.searchCount("inquiry", prev_domain)
        const percentage = ((data - prev_data)/prev_data) * 100
        this.state.memberinquiry.percentage = percentage.toFixed(2)

    }

    async getConfirmMember(){
        let domain = [['member_status', '=', 'Confirm']]
        if (this.state.period > 0)
        {
            domain.push(['date', '>', this.state.current_date])
        }
        const data = await this.orm.searchCount("inquiry", domain)
        this.state.memberconfirm.value = data

        // previous period
        let prev_domain = [['member_status', '=', 'Confirm']]
        if (this.state.period > 0){
            prev_domain.push(['date','>', this.state.previous_date], ['date','<=', this.state.current_date])
        }
        const prev_data = await this.orm.searchCount("inquiry", prev_domain)
        const percentage = ((data - prev_data)/prev_data) * 100
        this.state.memberconfirm.percentage = percentage.toFixed(2)

    }

    async getSales(){
        let domain = []
        if (this.state.period > 0)
        {
            domain.push(['date_of_sale', '>', this.state.current_date])
        }
        const data = await this.orm.searchCount("gym.sale.order", domain)

        // previous period
        let prev_domain = []
        if (this.state.period > 0){
            prev_domain.push(['date_of_sale','>', this.state.previous_date], ['date_of_sale','<=', this.state.current_date])
        }
        const prev_data = await this.orm.searchCount("gym.sale.order", prev_domain)
        const percentage = ((data - prev_data)/prev_data) * 100
         //sales amount
        const current_sales_amount = await this.orm.readGroup("gym.sale.order", domain, ["grand_total:sum"], [])
        const prev_sales_amount = await this.orm.readGroup("gym.sale.order", prev_domain, ["grand_total:sum"], [])
        const sales_amount_percentage = ((current_sales_amount[0].grand_total - prev_sales_amount[0].grand_total) / prev_sales_amount[0].grand_total) * 100

        this.state.sales = {
            value: data,
            percentage: percentage.toFixed(2),
            amount: `₹${(current_sales_amount[0].grand_total/1000).toFixed(2)}K`,
            amount_percentage: sales_amount_percentage.toFixed(2),
        }

    }

    async getPurchase(){
        let domain = []
        if (this.state.period > 0)
        {
            domain.push(['date', '>', this.state.current_date])
        }
        const data = await this.orm.searchCount("order", domain)

        // previous period
        let prev_domain = []
        if (this.state.period > 0){
            prev_domain.push(['date','>', this.state.previous_date], ['date','<=', this.state.current_date])
        }
        const prev_data = await this.orm.searchCount("order", prev_domain)
        const percentage = ((data - prev_data)/prev_data) * 100
         //purchase amount
        const current_purchase_amount = await this.orm.readGroup("order", domain, ["grand_total:sum"], [])
        const prev_purchase_amount = await this.orm.readGroup("order", prev_domain, ["grand_total:sum"], [])
        const purchase_amount_percentage = ((current_purchase_amount[0].grand_total - prev_purchase_amount[0].grand_total) / prev_purchase_amount[0].grand_total) * 100
        console.log("=============+>>> current_purchase_amount ::: ", current_purchase_amount)
        console.log("=============+>>> prev_purchase_amount ::: ", prev_purchase_amount)
        console.log("=============+>>> purchase_amount_percentage ::: ", purchase_amount_percentage)
        this.state.purchase = {
            value: data,
            percentage: percentage.toFixed(2),
            amount: `₹${(current_purchase_amount[0].grand_total/1000).toFixed(2)}K`,
            amount_percentage: purchase_amount_percentage.toFixed(2),
        }

    }

    viewInquiry(){
        let domain = [['member_status', 'in', ['Interested', 'Rejected']]]
        if (this.state.period > 0)
        {
            domain.push(['date', '>', this.state.current_date])
        }

        this.actionService.doAction({
            type: "ir.actions.act_window",
            name: "Inquiry",
            res_model: "inquiry",
            domain,
            views: [
                [false, "list"],
                [false, "form"],
            ]
        })
    }

    viewConfirmMember(){
        let domain = [['member_status', '=', 'Confirm']]
        if (this.state.period > 0)
        {
            domain.push(['date', '>', this.state.current_date])
        }

        this.actionService.doAction({
            type: "ir.actions.act_window",
            name: "Inquiry",
            res_model: "inquiry",
            domain,
            views: [
                [false, "list"],
                [false, "form"],
            ]
        })
    }

    viewSales(){
        let domain = []
        if (this.state.period > 0)
        {
            domain.push(['date_of_sale', '>', this.state.current_date])
        }

        this.actionService.doAction({
            type: "ir.actions.act_window",
            name: "Sales",
            res_model: "gym.sale.order",
            domain,
            views: [
                [false, "list"],
                [false, "form"],
            ]
        })
    }

    viewPurchase(){
        let domain = []
        if (this.state.period > 0)
        {
            domain.push(['date', '>', this.state.current_date])
        }

        this.actionService.doAction({
            type: "ir.actions.act_window",
            name: "Purchase",
            res_model: "order",
            domain,
            views: [
                [false, "list"],
                [false, "form"],
            ]
        })
    }
}

GymDashboard.template = "gym.GymDashboard"
GymDashboard.components = { Card }

registry.category("actions").add("gym.gym_dashboard", GymDashboard)
